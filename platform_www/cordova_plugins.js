cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-btprinter.BluetoothPrinter",
    "file": "plugins/cordova-plugin-btprinter/www/BluetoothPrinter.js",
    "pluginId": "cordova-plugin-btprinter",
    "clobbers": [
      "BTPrinter"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-geolocation.geolocation",
    "file": "plugins/cordova-plugin-geolocation/www/android/geolocation.js",
    "pluginId": "cordova-plugin-geolocation",
    "clobbers": [
      "navigator.geolocation"
    ]
  },
  {
    "id": "cordova-plugin-geolocation.PositionError",
    "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
    "pluginId": "cordova-plugin-geolocation",
    "runs": true
  },
  {
    "id": "cordova-plugin-nativeclicksound.nativeclick",
    "file": "plugins/cordova-plugin-nativeclicksound/www/nativeclick.js",
    "pluginId": "cordova-plugin-nativeclicksound",
    "clobbers": [
      "nativeclick"
    ]
  },
  {
    "id": "cordova-plugin-printer.Printer",
    "file": "plugins/cordova-plugin-printer/www/printer.js",
    "pluginId": "cordova-plugin-printer",
    "clobbers": [
      "plugin.printer",
      "cordova.plugins.printer"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "cordova-plugin-statusbar.statusbar",
    "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
    "pluginId": "cordova-plugin-statusbar",
    "clobbers": [
      "window.StatusBar"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-browsersync": "0.1.7",
  "cordova-plugin-btprinter": "0.0.1-dev",
  "cordova-plugin-crypt-file": "1.3.3",
  "cordova-plugin-device": "2.0.3-dev",
  "cordova-plugin-geolocation": "4.0.2",
  "cordova-plugin-nativeclicksound": "0.0.4",
  "cordova-plugin-printer": "0.7.3",
  "cordova-plugin-splashscreen": "5.0.2",
  "cordova-plugin-statusbar": "2.4.2",
  "cordova-plugin-whitelist": "1.3.3"
};
// BOTTOM OF METADATA
});