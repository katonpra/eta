/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.iknus.etapos;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.iknus.etapos";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 105;
  public static final String VERSION_NAME = "0.1.4";
}
