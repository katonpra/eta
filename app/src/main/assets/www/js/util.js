document.addEventListener("deviceready",function(){
	cekHash();
	StatusBar.backgroundColorByHexString("#536C78");
	$("#loading, #ld").hide();
	if(cekSupport()){
		initApp();
	}
});
// $('#pincode-input1').pincodeInput({inputs:6,hidedigits:false});
function clickSound() {
	$("body").click(function (e) {
		nativeclick.trigger();
	});
}
var Latitude = undefined;
var Longitude = undefined;
 var onSuccess = function(position) {
        Latitude = position.coords.latitude ;
        Longitude = position.coords.longitude;
    };
       function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }
navigator.geolocation.getCurrentPosition(onSuccess, onError);

var i=0;
function cekHash() {
	var p = getUrlVars()[0];
	if(p=='navbar'){
		$("#menu").hide();
		$("#back").show();
		if (!$('body').hasClass('overlay-open')) {
			window.history.go(-1);
		}else{
			if(i==0){
			$(".overlay-open .overlay").click(function(e) {
				window.history.go(-1);
			});
			i++;
			}
			$("#menu").show();
			$("#back").hide();
		}
	}else{
		if ($('body').hasClass('overlay-open')) {
			$('.overlay').hide();
			$('body').removeClass('overlay-open');
		}
		loadPage();
	}
}

function loadPage() {
	$('.page').hide();
	var p = getUrlVars()[0];
	var uid = window.localStorage.getItem('ID');
	var fr = window.localStorage.getItem('fr');
	$("li[class='active']").removeAttr('class');
	if (typeof (p) == 'undefined' || p == 'http:' || p == 'file:' || p == 'home') {
		$("a[href='#home']").closest('li').addClass('active');
		$('div[data-page="index"]').show();
		initRoute('home');
		$(".navbar-brand").text('Etalaseku').css('text-transform','none');
		$("#menu, #leftsidebar").show();
		$("#back").hide();
	} else {
		var lht = getUrlVars()['lihat'];
		var edt = getUrlVars()['edit'];
		var hps = getUrlVars()['hapus'];
		var add = getUrlVars()['add'];
		var idd = getUrlVars()['baca'];
		$("a[href='#" + p + "']").closest('li').addClass('active');
		$('div[data-page="'+p+'"]').show();
		initRoute(p);
		var ttl = p.split('-').join(' ');
		$(".navbar-brand").text(ttl).css('text-transform','capitalize');
		$("#menu, #leftsidebar").hide();
		$("#back").show();
	}
}

function getUrlVars() {
	var vars = [],
		hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('/');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}

function notif(pesan) {
	$.notify({
		message: pesan
	}, {
		allow_dismiss: true,
		newest_on_top: true,
		timer: 1500,
		delay: 1500,
		placement: {
			from: 'top',
			align: 'right'
		},
		animate: {
			enter: 'animated bounceInRight',
			exit: 'animated bounceOutRight'
		}
	});
}

function initComponent() {
	$('.back').click(function (e) {
		window.history.go(-1);
	});
	$.AdminBSB.input.activate();
	$.AdminBSB.DatePicker.activate();
	$.AdminBSB.select.activate();
	FastClick.attach(document.body);
	noAutoComplete();
}

function noAutoComplete() {
	$('input').attr('autocomplete', 'off');
}

function getFr() {
	var client = new ClientJS();
	var fingerprint = client.getFingerprint();
	window.localStorage.setItem('fr', fingerprint);
}

function animDiv(elm) {
	var animationName = 'tada';
	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	$(elm).addClass('animated ' + animationName).one(animationEnd, function () {
		$(elm).removeClass('animated ' + animationName);
	});
}

function autoHeight(){
	var h = window.innerHeight;
	h=h-220;
	// $("#l_cartd").css('height',(h-100));
	// $("#lpproduk").css('height',h);
	$("#lproduk, #ltrx").css('height',(h+45));
	$("#lproduk, #lpproduk, #ltrx, #l_cartd").css('overflow-y','scroll').css('overflow-x','hidden');
}
function openProv() {
				$.ajax({
                url : urlA+'registrasi/GetProv',
                type : "POST",
                headers: {
                  'Screet': '63106676e60eec428c8a06bebc47dda7',
                  'Content-Type':'application/x-www-form-urlencoded',
                  'Accept':'APIQ4.0'
                },
                dataType : 'JSON',
				cache: false,
                success: function(respon){		
					console.log("RESPONSE PROVINSI >>>>", respon);
                    var html = '<option disabled selected>Pilih Provinsi</option>';
                    var i;
                    for(i=0; i<respon.Data.length; i++){
                        html += '<option value="'+respon.Data[i].prov+'">'+respon.Data[i].prov+'</option>';
                    }
                    $('#prov').html(html);
                     
                	}
                 });
			}
			$('#prov').change(function(){
            	var id=$(this).val();
		            $.ajax({
		                url : urlA+'registrasi/GetKota',
		                headers: {
		                  'Screet': '63106676e60eec428c8a06bebc47dda7',
		                  'Content-Type':'application/x-www-form-urlencoded',
		                  'Accept':'APIQ4.0'
		                },
		                type : "POST",
		                dataType : 'JSON',
						cache: false,
						data: {"Prov":id},
		                success: function(response){
		                  console.log("kota  >>>",response);
		                      var html = '<option disabled>Pilih Kota</option>';
		                      var i;
		                      for(i=0; i<response.Data.length; i++){
		                        html += '<option value="'+response.Data[i].kabupaten+'">'+response.Data[i].kabupaten+'</option>';
		                    }
		                    $('#kota').html(html);
		                  }
		                  });
	              });
			$('#kota').change(function(){
	            var id=$(this).val();
	            $.ajax({
	                url : urlA+'registrasi/GetKec',
	                headers: { 
		                  'Screet': '63106676e60eec428c8a06bebc47dda7',
		                  'Content-Type':'application/x-www-form-urlencoded',
		                  'Accept':'APIQ4.0'
	                },
	                type : "POST",
	                dataType : 'JSON',
					cache: false,
					data: {"Kota":id},
	                success: function(response){
	                  console.log("kecamatan  >>>",response);
	                      var html = '<option disabled>Pilih Kecamatan</option>';
	                      var i;
	                      for(i=0; i<response.Data.length; i++){
	                        html += '<option value="'+response.Data[i].kecamatan+'">'+response.Data[i].kecamatan+'</option>';
	                    }
	                    $('#kec').html(html);
	                  }
	                  });
	              });
			$('#kec').change(function(){
	            var id=$(this).val();
	            $.ajax({
	                url : urlA+'registrasi/GetDesa',
	                type : "POST",
	                headers: {
		                  'Screet': '63106676e60eec428c8a06bebc47dda7',
		                  'Content-Type':'application/x-www-form-urlencoded',
		                  'Accept':'APIQ4.0'
	                },
	                dataType : 'JSON',
					cache: false,
					data: {"Kec":id},
	                success: function(response){
	                  console.log("desa >>>",response);
	                      var html = '<option disabled>Pilih Desa</option>';
	                      var i;
	                      for(i=0; i<response.Data.length; i++){
	                        html += '<option value="'+response.Data[i].desa+'">'+response.Data[i].desa+'</option>';
	                    }
	                    $('#desa').html(html);
	                  }
	                  });
	              });
		
